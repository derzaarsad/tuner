/*
 * main.c
 */
#include "tunercfg.h"
#include <csl.h>
#include <csl_mcbsp.h>
#include <csl_irq.h>
#include <csl_edma.h>
#include <dsk6713.h>
#include <dsk6713_led.h>
#include "config_AIC23.h"
#include "skeleton.h"
#include <math.h>
#include "utility.h"
#include <stdlib.h>
#include <DSPF_sp_cfftr2_dit.h>

#define PI 3.14159265

#define E4 329.63
#define H3 246.94
#define G3 196.0
#define D3 146.83
#define A2 110.0
#define E2 82.41

void config_EDMA(void);
void config_interrupts(void);
void EDMA_interrupt_service(void);
void process_ping_SWI(void);
void process_pong_SWI(void);
void SWI_LEDToggle(void);
void tsk_led_toggle(void);
void FFT_calc(void);
void x_array_calc(int nbr);
void init_hamming(void);
void areaHamm(void);
void Lxx_calc(void);
void Maxima(float *max_Value, int *max_Index);
float noise_calc(void);
void tone_detect(void);
void led_disp(void);
int harmon(float fmax);

int fensternbr = 0; // 0,1,2
volatile int fenstercnt = 0; // 16384
float W=0; // Area under Hamming
float SampleRate = 48000;
float deltaF = 2.92969; //Samplerate/FENSTER_LEN
float fref = A2;	//Referenzfrequenz, vom Benutzer definiert

main()
{
	CSL_init();  /* init CSL */

	/* Konfiguration des AIC23 �ber McBSP0 */
	Config_DSK6713_AIC23();

	/* Konfiguration des McBSP1 - Datenschnittstelle */
	hMcbsp = MCBSP_open(MCBSP_DEV1, MCBSP_OPEN_RESET);
    MCBSP_config(hMcbsp, &datainterface_config);

    config_EDMA();

    /* Initialisieren Hamming Koeffizient und Berechnen die Fl�che */
    init_hamming();
    areaHamm();

    /* Initialisieren FFT Koeffizient */
    /* twiddle factor Berechnung */
    tw_genr2fft(w,FENSTER_LEN);
    /* bit reverse von twiddle factor */
    bit_rev(w,FENSTER_LEN >>1);

    /* interrupts immer zuletzt */
    config_interrupts();

    MCBSP_start(hMcbsp, MCBSP_XMIT_START | MCBSP_RCV_START, 0xffffffff);
    MCBSP_write(hMcbsp, 0x0); /* einmal schie�en zum starten*/

} /* und fertig */

void config_EDMA(void)
{
	/* Konfiguration der EDMA zum Lesen*/
	hEdmaRcv = EDMA_open(EDMA_CHA_REVT1, EDMA_OPEN_RESET);  // EDMA Kanal f�r das Event REVT1
	hEdmaReloadRcvPing = EDMA_allocTable(-1);               // einen Reload-Parametersatz
	hEdmaReloadRcvPong = EDMA_allocTable(-1);

	configEDMARcv.src = MCBSP_getRcvAddr(hMcbsp);          //  Quell-Adresse zum Lesen

	tccRcvPing = EDMA_intAlloc(-1);                        // n�chsten freien Transfer-Complete-Code
	configEDMARcv.opt |= EDMA_FMK(OPT,TCC,tccRcvPing);     // dann der Grundkonfiguration des EDMA Empfangskanals zuweisen
	configEDMARcv.dst = EDMA_DST_OF(Buffer_in_ping);

	/* ersten Transfer und Reload-Ping mit ConfigPing konfigurieren */
	EDMA_config(hEdmaRcv, &configEDMARcv);
	EDMA_config(hEdmaReloadRcvPing, &configEDMARcv);

	tccRcvPong = EDMA_intAlloc(-1);						   // n�chsten freien Transfer-Complete-Code
	configEDMARcv.opt &= 0xfff0ffff;
	configEDMARcv.opt |= EDMA_FMK(OPT,TCC,tccRcvPong);     // dann der Grundkonfiguration des EDMA Empfangskanals zuweisen
	configEDMARcv.dst = EDMA_DST_OF(Buffer_in_pong);

	EDMA_config(hEdmaReloadRcvPong, &configEDMARcv);
	//EDMA-Konfigurationen fuer das Lesen pong (andere zieladresse Buffer_in_pong, tccRcvPong,link) */
	EDMA_link(hEdmaRcv,hEdmaReloadRcvPong);                 // link the regs to Pong
    EDMA_link(hEdmaReloadRcvPong,hEdmaReloadRcvPing);       // link Pong to Ping
    EDMA_link(hEdmaReloadRcvPing,hEdmaReloadRcvPong);       // and link Ping to Pong


	/* EDMA fuer schreiben   */
	hEdmaXmt = EDMA_open(EDMA_CHA_XEVT1, EDMA_OPEN_RESET);
	hEdmaReloadXmtPing = EDMA_allocTable(-1);
	hEdmaReloadXmtPong = EDMA_allocTable(-1);

	configEDMAXmt.dst = MCBSP_getXmtAddr(hMcbsp);

	tccXmtPing = EDMA_intAlloc(-1);
	configEDMAXmt.src = EDMA_SRC_OF(Buffer_out_ping);
	configEDMAXmt.opt |= EDMA_FMK(OPT,TCC,tccXmtPing);

	EDMA_config(hEdmaXmt, &configEDMAXmt);
	EDMA_config(hEdmaReloadXmtPing, &configEDMAXmt);

	tccXmtPong = EDMA_intAlloc(-1);
	configEDMAXmt.src = EDMA_SRC_OF(Buffer_out_pong);
	configEDMAXmt.opt &= 0xfff0ffff;
	configEDMAXmt.opt |= EDMA_FMK(OPT,TCC,tccXmtPong);

	EDMA_config(hEdmaReloadXmtPong, &configEDMAXmt);

	EDMA_link(hEdmaXmt,hEdmaReloadXmtPong);                 // link the regs to Pong // void EDMA_link(EDMA_Handle parent, EDMA_Handle child); parent link from; child link to
    EDMA_link(hEdmaReloadXmtPong,hEdmaReloadXmtPing);       // link Pong to Ping
    EDMA_link(hEdmaReloadXmtPing,hEdmaReloadXmtPong);       // and link Ping to Pong

	/* EDMA TCC-Interrupts freigeben */
	EDMA_intClear(tccRcvPing);
	EDMA_intEnable(tccRcvPing);
	/* schreiben interrupt freigeben*/
	EDMA_intClear(tccXmtPing);
	EDMA_intEnable(tccXmtPing);

	EDMA_intClear(tccRcvPong);
	EDMA_intEnable(tccRcvPong);

	EDMA_intClear(tccXmtPong);
	EDMA_intEnable(tccXmtPong);

	/* EDMA starten */
	EDMA_enableChannel(hEdmaRcv);
	EDMA_enableChannel(hEdmaXmt);

	MCBSP_write(hMcbsp, 0);
}


void config_interrupts(void)
{
	IRQ_map(IRQ_EVT_EDMAINT, 8); //edma 8
	IRQ_clear(IRQ_EVT_EDMAINT);
	IRQ_enable(IRQ_EVT_EDMAINT);
	IRQ_globalEnable();
}


void EDMA_interrupt_service(void) //mit underscore angemeldet
{
	static int rcvPingDone=0;
	static int rcvPongDone=0;
	static int xmtPingDone=0;
	static int xmtPongDone=0;


	if(EDMA_intTest(tccRcvPing)) {		//von wem ist der interrupt?
		EDMA_intClear(tccRcvPing); /* ohne clear geht es NICHT */
		rcvPingDone=1;
	}
	else if(EDMA_intTest(tccRcvPong)) {
		EDMA_intClear(tccRcvPong);
		rcvPongDone=1;
	}

	if(EDMA_intTest(tccXmtPing)) {
		EDMA_intClear(tccXmtPing);
		xmtPingDone=1;
	}
	else if(EDMA_intTest(tccXmtPong)) {
		EDMA_intClear(tccXmtPong);
		xmtPongDone=1;
	}

	if(rcvPingDone && xmtPingDone) {
		rcvPingDone=0;
		xmtPingDone=0;
		// SW-Interrupt mit Verarbeitung
		SWI_post(&SWI_ping);
	}
	else if(rcvPongDone && xmtPongDone) {
		rcvPongDone=0;
		xmtPongDone=0;
		// SW-Interrupt mit Verarbeitung
		SWI_post(&SWI_pong);
	}
}

void process_ping_SWI(void)
{
	int i;
	int k;

	/* Eingabe parsen, eingabe ist Stereo, so nur Elemente mit gerade Indexzahl werden genommen */
	for(i=0; i<BUFFER_LEN/2; i++){
		/* Schreib nach Output-Buffer, Nicht Relevant mit der Berechnung */
		*(Buffer_out_ping+i) = *(Buffer_in_ping+2*i);

		/* Fenster ausf�llen */
		Fenster[fensternbr][fenstercnt] = *(Buffer_in_ping+2*i);
		fenstercnt++;

		if(fenstercnt == FENSTER_LEN){
			fenstercnt = 0;
			fensternbr++;

			/* Wenn das Fenster ausgef�llt ist, startet den Prozess  */
			if(fensternbr == 3)
			{
				/* Alle LEDs an w�hrend Berechnung */
				DSK6713_LED_on(0);
				DSK6713_LED_on(1);
				DSK6713_LED_on(2);
				DSK6713_LED_on(3);

				/* 1. Fenster ins x-Array schreiben, x-Array wird ist ein Buffer zur FFT-Berechnung */
				x_array_calc(1);
				/* FFT berechnen */
				FFT_calc();

				/* 2. Fenster FFT */
				x_array_calc(2);
				FFT_calc();

				/* 3. Fenster FFT */
				x_array_calc(3);
				FFT_calc();

				/* 4. Fenster FFT */
				x_array_calc(4);
				FFT_calc();

				/* 5. Fenster FFT */
				x_array_calc(5);
				FFT_calc();

				/* Nach 5 Fenstern spektrale Leistungsdichte berechnen */
				Lxx_calc();

				/* Ton-Detektion und LED Ansteuerung */
				tone_detect();

				/* Reset Aufsummierungsfenster */
				for(k=0;k<FENSTER_LEN; k++)
				{
					Lxx[k] = 0.0;
					per[k] = 0.0;
				}
				fensternbr = 0;			// Einlesen wieder starten
			}
		}
	}
}

void process_pong_SWI(void)			// das Gleiche f�r pong
{
	int i;
	int k;

	for(i=0; i<BUFFER_LEN/2; i++){
		*(Buffer_out_pong+i) = *(Buffer_in_pong+2*i);
		Fenster[fensternbr][fenstercnt] = *(Buffer_in_pong+2*i);
		fenstercnt++;

		if(fenstercnt == FENSTER_LEN){
				fenstercnt = 0;
				fensternbr++;
				if(fensternbr == 3)
				{
					DSK6713_LED_on(0);
					DSK6713_LED_on(1);
					DSK6713_LED_on(2);
					DSK6713_LED_on(3);
					x_array_calc(1);
					FFT_calc();
					x_array_calc(2);
					FFT_calc();
					x_array_calc(3);
					FFT_calc();
					x_array_calc(4);
					FFT_calc();
					x_array_calc(5);
					FFT_calc();
					Lxx_calc();
					tone_detect();
					for(k=0;k<FENSTER_LEN; k++){
						Lxx[k] = 0.0;
						per[k] = 0.0;
					}
					fensternbr = 0;
				}
			}
	}

}

void SWI_LEDToggle(void)
{
	SEM_postBinary(&SEM_LEDToggle);

}

void init_hamming(void){		//hamming Funktion f�r Koeffizienten
	int i=0;
	float var;
	for(i=0; i<FENSTER_LEN; i++){
		var = (2*i*PI)/(FENSTER_LEN - 1);
		hammingCoef[i] = 0.54 - (0.46*cos(var));
	}
}

void areaHamm(void){		  //Fl�che unter Hamming (Leistung)
	int i=0;
	for(i=0; i<FENSTER_LEN; i++){
		W += hammingCoef[i]*hammingCoef[i];
	}
	W = W/FENSTER_LEN;
}

void x_array_calc(int nbr){		//Fenster in x Array schreiben, abwechselnd Re und Im Teil, Im-Teil ist 0
	int i = 0;

	switch(nbr)
	{
		case 1:								// Fenster 1
			for (i = 0; i < 16384; i++)
			{
				x[2*i] = (float) Fenster[0][i]*hammingCoef[i];
				x[2*i + 1] = 0.0;
			}
			break;
		case 2:								// 1. �berlappendes Fenster
			for (i = 0; i < 8192; i++)
			{
				x[2*i] = (float) Fenster[0][i+8192]*hammingCoef[i];
				x[2*i + 1] = 0.0;
			}
			for (i = 8192; i < 16384; i++)
			{
				x[2*i] = (float) Fenster[1][i-8192]*hammingCoef[i];
				x[2*i + 1] = 0.0;
			}
			break;
		case 3:								// Fenster 2
			for (i = 0; i < 16384; i++)
			{
				x[2*i] = (float) Fenster[1][i]*hammingCoef[i];
				x[2*i + 1] = 0.0;
			}
			break;
		case 4:								// 2. �berlappendes Fenster
			for (i = 0; i < 8192; i++)
			{
				x[2*i] = (float) Fenster[1][i+8192]*hammingCoef[i];
				x[2*i + 1] = 0.0;
			}
			for (i = 8192; i < 16384; i++)
			{
				x[2*i] = (float) Fenster[2][i-8192]*hammingCoef[i];
				x[2*i + 1] = 0.0;
			}
			break;
		case 5:								// Fenster 3
			for (i = 0; i < 16384; i++)
			{
				x[2*i] = (float) Fenster[2][i]*hammingCoef[i];
				x[2*i + 1] = 0.0;
			}
			break;
		default:
			break;
	}
}

void FFT_calc(void){			//FFT Berechnung
	int i=0;
	DSPF_sp_cfftr2_dit(x,w,FENSTER_LEN);		//output ist bitreversed
	bit_rev(x,FENSTER_LEN);						//output richtig anordnen

	for(i=0; i<FENSTER_LEN; i++)
	{
		per[i] += (x[2*i]*x[2*i]+x[2*i+1]*x[2*i+1])/FENSTER_LEN;		//per[i] = (1/FENSTER_LEN) * mag[i]*mag[i];
																		// Summme: for(k=0; k<5; k++)
	 																	//memset(Lxx, 0, sizeof(float)*FENSTER_LEN);
	}

}

void Lxx_calc(void)		//PSD
{
	int i=0;
	for(i=0; i<FENSTER_LEN; i++)
	{
		Lxx[i] = 1/W*1/5*(per[i]);
	}
}

void Maxima(float *max_Value, int *max_Index){		//Frequenz von Maximum suchen
	int i=0;
	int start_Index;
	int end_Index;

	/* Maximale Frequenz im Bereich fref-50Hz < f < fref+50Hz �berpr�fen */
	start_Index = (int)((fref-50.0)/deltaF);
	end_Index = (int)((fref+50.0)/deltaF);

	for(i = start_Index; i <= end_Index; i++)
	{
		if(Lxx[i]>*max_Value)
		{
			*max_Value = Lxx[i];
			*max_Index = i;
		}
	}

}

int harmon(float fmax) {			//gibt es Harmonische?
	/* Init variable */
	int i=0;
	int start_Index;
	int end_Index;
	float max_Value = 0.0;
	int max_Index = 0;
	float fh1 = 0.0;
	float fh2 = 0.0;
	int tone = 0;

	/* Oberwelle im Bereich 2*fmax-20Hz < f < 2*fmax+20Hz �berpr�fen */
	start_Index = (int)((2*fmax-20.0)/deltaF);
	end_Index = (int)((2*fmax+20.0)/deltaF);

	for(i = start_Index; i <= end_Index; i++)
	{
		if(Lxx[i] > max_Value) {
			max_Value = Lxx[i];
			max_Index = i;
		}
	}
	fh1 = max_Index*deltaF;

	/* Wenn die Oberwelle die Voraussetzung von 1. Oberwelle erf�llt, �berpr�ft 2. Oberwelle */
	if((fh1<fmax*2+10)&&(fh1>fmax*2-10)) {
		/* Oberwelle im Bereich 3*fmax-20Hz < f < 3*fmax+20Hz �berpr�fen */
		start_Index = (int)((3*fmax-20.0)/deltaF);
		end_Index = (int)((3*fmax+20.0)/deltaF);
		max_Value = 0.0;
		max_Index = 0;
		for(i = start_Index; i <= end_Index; i++)
		{
			if(Lxx[i]>max_Value) {
				max_Value = Lxx[i];
				max_Index = i;
			}
		}
		fh2 = max_Index*deltaF;
		if((fh2<fmax*3+10)&&(fh2>fmax*3-10)){
			tone = 1;
		}
	}
	return tone;

}

float noise_calc(void){			//Rauschen von 20kHz bis 24kHz, spektrale Index 6827 bis 8192
	int i=0;
	float noiseLevel = 0.0;
	for(i=6827; i<=8192; i++){
		noiseLevel += Lxx[i];
	}
	noiseLevel = noiseLevel/1365;
	return noiseLevel;
}

void tone_detect(void) {
	/* Init variable */
	float max_Value = 0.0;
	int max_Index = 0;
	float noise = 0.0;
	int validation_lvl =0;
	int tone = 0;
	float fmax =  0.0;

	/* Sucht maximalen Wert von spektraler Leistungsdichte */
	Maxima(&max_Value, &max_Index);

	/* Rauschleistung berechnen */
	noise = noise_calc();
	if(noise<20) {
		noise= 20;
	}

	/* Led erstmal aus */
	DSK6713_LED_off(0);
	DSK6713_LED_off(1);
	DSK6713_LED_off(2);
	DSK6713_LED_off(3);

	/* Vergleich der maximalen Wert mit dem Rauschen */
	if(max_Value >= 5*noise) {
		fmax = max_Index*deltaF;
		validation_lvl = 1;
	}

	/* Oberwellen �berpr�fen */
	if((Lxx[2*max_Index] >= 5*noise)) {		//&&(fmax>70)
		if(harmon(fmax) == 1) {
			validation_lvl = validation_lvl + 2;
		}
	}

	/* Schaltet LED ein */
	if(validation_lvl == 3) {
		if((fmax>fref-5)&&(fmax<fref+5)) {
			DSK6713_LED_on(0);
			DSK6713_LED_on(1);
			DSK6713_LED_on(2);
			DSK6713_LED_on(3);
		}
		if((fmax<fref-5)&&(fmax>fref-20)) {
			 DSK6713_LED_on(1);
		}
		if((fmax>fref+5)&&(fmax<fref+20)) {
			DSK6713_LED_on(2);
		}
		if(fmax<fref-20) {
			DSK6713_LED_on(0);
		}
		if(fmax>fref+20) {
			DSK6713_LED_on(3);
		}
		tone = 1;
	}
}


void tsk_led_toggle(void)
{
	/* Initialisierung der Task */
	/* zu Testzwecken */
	DSK6713_LED_off(0);
	DSK6713_LED_off(1);
	DSK6713_LED_off(2);
	DSK6713_LED_off(3);

	/* Verarbeitung */

	while(1) {
		SEM_pendBinary(&SEM_LEDToggle, SYS_FOREVER);

		//DSK6713_LED_toggle(0);

	}

	/* Aufr�umen */
	/* nix */
}


