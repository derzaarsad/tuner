################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../tun.cmd 

TCF_SRCS += \
../tuner.tcf 

LIB_SRCS += \
../dsk6713bsl.lib 

S??_SRCS += \
./tunercfg.s?? 

C_SRCS += \
../config_AIC23.c \
../main.c \
./tunercfg_c.c \
../utility.c 

OBJS += \
./config_AIC23.obj \
./main.obj \
./tunercfg.obj \
./tunercfg_c.obj \
./utility.obj 

GEN_MISC_FILES += \
./tuner.cdb 

GEN_HDRS += \
./tunercfg.h \
./tunercfg.h?? 

S??_DEPS += \
./tunercfg.pp 

C_DEPS += \
./config_AIC23.pp \
./main.pp \
./tunercfg_c.pp \
./utility.pp 

GEN_CMDS += \
./tunercfg.cmd 

GEN_FILES += \
./tunercfg.cmd \
./tunercfg.s?? \
./tunercfg_c.c 

GEN_HDRS__QUOTED += \
"tunercfg.h" \
"tunercfg.h??" 

GEN_MISC_FILES__QUOTED += \
"tuner.cdb" 

GEN_FILES__QUOTED += \
"tunercfg.cmd" \
"tunercfg.s??" \
"tunercfg_c.c" 

C_DEPS__QUOTED += \
"config_AIC23.pp" \
"main.pp" \
"tunercfg_c.pp" \
"utility.pp" 

S??_DEPS__QUOTED += \
"tunercfg.pp" 

OBJS__QUOTED += \
"config_AIC23.obj" \
"main.obj" \
"tunercfg.obj" \
"tunercfg_c.obj" \
"utility.obj" 

C_SRCS__QUOTED += \
"../config_AIC23.c" \
"../main.c" \
"./tunercfg_c.c" \
"../utility.c" 

GEN_CMDS__FLAG += \
-l"./tunercfg.cmd" 

S??_SRCS__QUOTED += \
"./tunercfg.s??" 

S??_OBJS__QUOTED += \
"tunercfg.obj" 


